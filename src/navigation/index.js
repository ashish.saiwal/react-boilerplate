import React, {Component, Fragment} from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import PageNotFound from "../containers/PageNotFound";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Landing from "../containers/Landing";


class Navigation extends Component {
    render() {
        return (
            <Fragment>
                <ToastContainer
                    position="bottom-center"
                    autoClose={5000}
                    hideProgressBar
                    newestOnTop
                    closeOnClick={false}
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/" component={Landing}/>
                        <Route component={PageNotFound} />
                    </Switch>
                </BrowserRouter>
            </Fragment>
        );
    }
}


export default Navigation;
