import React from 'react';
import ReactDOM from 'react-dom';
import Navigation from "./navigation";
import reportWebVitals from './reportWebVitals';

import './styles/Global.scss'
// Load the navigation stack
ReactDOM.render(
    <React.StrictMode>
        <Navigation/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
