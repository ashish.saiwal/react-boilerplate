import React from 'react';
import PropTypes from 'prop-types';
import './Styles.scss';

const PrimaryButton = (props) => {
    const {style, text, textStyle} = props;
    return (
        <div className={"primary-button-container"} style={style} {...props}>
            <div className={"button-text"} style={textStyle}>
                {text}
            </div>
        </div>
    )
}

PrimaryButton.propTypes = {
    style: PropTypes.object,
    text: PropTypes.string,
    textStyle: PropTypes.object
};

export default PrimaryButton;
