import axios from "axios";
import config from "../../config";

const Axios = axios.create({
    baseURL: config.BASE_URL
});


Axios.interceptors.request.use(async (config) => {
    try {
        const token = sessionStorage.getItem('token')
        if (token) {
            config.headers.Authorization = token;
        }
    } catch (e) {
        console.error(e)
    }
    return config;
})

Axios.interceptors.response.use(
    res => res,
    err => {
        if (err.response.status === 404) {
            window.location.href='/'
        }
        throw err.response.data;
    }
);


export default Axios
