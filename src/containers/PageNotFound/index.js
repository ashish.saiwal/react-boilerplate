import React, {Component} from 'react';
import {Link} from "react-router-dom";

class PageNotFound extends Component {
    render() {
        return (
            <div className={'text-center mt-5'}>
                <h1>
                    Page not found
                </h1>
                <Link to={'/'}>Back to home</Link>
            </div>
        );
    }
}


export default PageNotFound;
